﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Formularios
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonIngresar_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32 (textBoxId.Text);
            string nombre = textBoxNombre.Text;
            int edad = Convert.ToInt32 (textBoxEdad.Text);
            Conexion.agregarAlumno(id, nombre, edad);

        }

        private void buttonPruebas_Click(object sender, EventArgs e)
        {
            Conexion.pruebas();
            Console.WriteLine();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }


        private void buttonMostrar_Click(object sender, EventArgs e)
        {
        	listViewAlumnos.Clear();
        	List<Alumno> listaAlumnos= Conexion.listarAlumnosCompleto();
        	foreach (Alumno alumno in listaAlumnos){
        		String alu=alumno.ToString();
        		listViewAlumnos.Items.Add(alu);
        	}

        }
    }
}
