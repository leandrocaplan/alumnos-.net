﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;


namespace Formularios
{
    class Conexion
    {
        static SqlConnection myConnection;

        internal static ArrayList listarAlumnosArrayList()
        {
            ArrayList alumnos = new ArrayList();
            int cont = 0;
            SqlCommand myCommand = new SqlCommand("SELECT * FROM dbo.alumnos", myConnection);

            SqlDataReader myReader = myCommand.ExecuteReader(); //El reader es para iterar en los resultados

            while (myReader.Read())
            {
                Alumno alumno = new Alumno();

                alumno.Id = Convert.ToInt32(myReader["Id"].ToString());
                alumno.Nombre = myReader["nombre"].ToString();
                alumno.Edad = Convert.ToInt32(myReader["edad"].ToString());
                alumnos.Add(alumno);
                cont++;
            }
            myReader.Close();
            return alumnos;
        }
        
        internal static List<String> listarAlumnos()
        {
            List<String> alumnos = new List<String>();

            SqlCommand myCommand = new SqlCommand("SELECT * FROM dbo.alumnos", myConnection);

            SqlDataReader myReader = myCommand.ExecuteReader(); //El reader es para iterar en los resultados

            while (myReader.Read())
            {
                alumnos.Add(myReader["nombre"].ToString());

            }
            myReader.Close();
            return alumnos;
        }
     
        
        internal static List<Alumno> listarAlumnosCompleto()
        {
            List<Alumno> alumnos = new List<Alumno>();


            SqlCommand myCommand = new SqlCommand("SELECT * FROM dbo.alumnos", myConnection);

            SqlDataReader myReader = myCommand.ExecuteReader(); //El reader es para iterar en los resultados

            while (myReader.Read())
            {
                Alumno alumno = new Alumno();

                alumno.Id = Convert.ToInt32(myReader["Id"].ToString());
                alumno.Nombre = myReader["nombre"].ToString();
                alumno.Edad = Convert.ToInt32(myReader["edad"].ToString());
                alumnos.Add(alumno);
            }
            myReader.Close();
            return alumnos;
        }

        internal static bool estaSubido(int id)
        {
            string comando = String.Format("SELECT * FROM dbo.alumnos WHERE Id={0}", id);
            SqlCommand myCommand = new SqlCommand(comando, myConnection);

            SqlDataReader myReader = myCommand.ExecuteReader();
            if (myReader.Read())
            {
                myReader.Close();
                return true;
            }
            
            myReader.Close();
            return false;


        }
        internal static void agregarAlumno(int id, String nombre, int edad)
        {
            String valores = String.Format("({0},'{1}',{2})", id, nombre, edad);
            SqlCommand myCommand = new SqlCommand("INSERT INTO dbo.alumnos (Id,nombre,edad) VALUES" + valores, myConnection);
            myCommand.ExecuteNonQuery();
        }

        internal static void modificarAlumno(int Id, string nuevoNombre)
        {
            SqlCommand myCommand = new SqlCommand("UPDATE dbo.alumnos SET nombre= " + nuevoNombre + " WHERE Id=" + Id);
            myCommand.ExecuteNonQuery();
        }
        internal static void conectar()
        {
            try
            {

                myConnection = new SqlConnection(@"user id=DESKTOP-JRRTL32\54113;" +
                                           @"password=;server=DESKTOP-JRRTL32;" +
                                           "Trusted_Connection=yes;" +
                                           "database=prueba; " +
                                           "connection timeout=30");
                myConnection.Open();
                
           }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());

            }           
        }

        internal static void desconectar() 
        {
            myConnection.Close();
        }
        internal static void pruebas()
        {
            List<String> listaAlumnos = Conexion.listarAlumnos();
            List<Alumno> listaAlumnosComp = new List<Alumno>();
            foreach (String alumno in listaAlumnos)
                Console.WriteLine(alumno);

            //agregarAlumno(342, "Jorge Gimenez", 24);
            if (Conexion.estaSubido(343))
                Console.WriteLine("Este padron está subido");
            else
                Console.WriteLine("Este padron no está subido");
            listaAlumnos = Conexion.listarAlumnos();
            Console.WriteLine();
            foreach (String alumno in listaAlumnos)
                Console.WriteLine(alumno);

            listaAlumnosComp = Conexion.listarAlumnosCompleto();
            foreach (Alumno alumno in listaAlumnosComp)
                Console.WriteLine(alumno);
            ArrayList listaAlumnosArrayList = new ArrayList();
            listaAlumnosArrayList = Conexion.listarAlumnosArrayList();
            for (int i = 0; i < listaAlumnosArrayList.Count; i++)
            {
                Console.WriteLine(listaAlumnosArrayList[i]);
            }
        }
    }
}