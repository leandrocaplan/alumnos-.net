﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Formularios
{
    class Alumno
    {

        public int Id { get; set; }
        public string Nombre { get; set; }
        public int Edad {get;set;}
        /*
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        public int Edad
        {
            get { return edad; }
            set { edad = value; }
        }
         */
        public Alumno() { }
        public Alumno(int Id, string Nombre, int Edad)
        {
            this.Id = Id;
            this.Nombre = Nombre;
            this.Edad = Edad;
        }

        public override string ToString()
        {
            return Id + " " + Nombre + " " + Edad;
        }

    }
}
