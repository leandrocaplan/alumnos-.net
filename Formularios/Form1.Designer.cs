﻿namespace Formularios
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
        	this.buttonIngresar = new System.Windows.Forms.Button();
        	this.labelId = new System.Windows.Forms.Label();
        	this.labelNombre = new System.Windows.Forms.Label();
        	this.labelEdad = new System.Windows.Forms.Label();
        	this.textBoxId = new System.Windows.Forms.TextBox();
        	this.textBoxNombre = new System.Windows.Forms.TextBox();
        	this.textBoxEdad = new System.Windows.Forms.TextBox();
        	this.listViewAlumnos = new System.Windows.Forms.ListView();
        	this.buttonMostrar = new System.Windows.Forms.Button();
        	this.SuspendLayout();
        	// 
        	// buttonIngresar
        	// 
        	this.buttonIngresar.Location = new System.Drawing.Point(183, 199);
        	this.buttonIngresar.Name = "buttonIngresar";
        	this.buttonIngresar.Size = new System.Drawing.Size(75, 23);
        	this.buttonIngresar.TabIndex = 0;
        	this.buttonIngresar.Text = "Ingresar";
        	this.buttonIngresar.UseVisualStyleBackColor = true;
        	this.buttonIngresar.Click += new System.EventHandler(this.buttonIngresar_Click);
        	// 
        	// labelId
        	// 
        	this.labelId.AutoSize = true;
        	this.labelId.Location = new System.Drawing.Point(45, 45);
        	this.labelId.Name = "labelId";
        	this.labelId.Size = new System.Drawing.Size(18, 13);
        	this.labelId.TabIndex = 1;
        	this.labelId.Text = "ID";
        	// 
        	// labelNombre
        	// 
        	this.labelNombre.AutoSize = true;
        	this.labelNombre.Location = new System.Drawing.Point(45, 86);
        	this.labelNombre.Name = "labelNombre";
        	this.labelNombre.Size = new System.Drawing.Size(44, 13);
        	this.labelNombre.TabIndex = 2;
        	this.labelNombre.Text = "Nombre";
        	// 
        	// labelEdad
        	// 
        	this.labelEdad.AutoSize = true;
        	this.labelEdad.Location = new System.Drawing.Point(45, 124);
        	this.labelEdad.Name = "labelEdad";
        	this.labelEdad.Size = new System.Drawing.Size(32, 13);
        	this.labelEdad.TabIndex = 3;
        	this.labelEdad.Text = "Edad";
        	// 
        	// textBoxId
        	// 
        	this.textBoxId.Location = new System.Drawing.Point(153, 37);
        	this.textBoxId.Name = "textBoxId";
        	this.textBoxId.Size = new System.Drawing.Size(100, 20);
        	this.textBoxId.TabIndex = 4;
        	// 
        	// textBoxNombre
        	// 
        	this.textBoxNombre.Location = new System.Drawing.Point(153, 86);
        	this.textBoxNombre.Name = "textBoxNombre";
        	this.textBoxNombre.Size = new System.Drawing.Size(100, 20);
        	this.textBoxNombre.TabIndex = 5;
        	// 
        	// textBoxEdad
        	// 
        	this.textBoxEdad.Location = new System.Drawing.Point(153, 121);
        	this.textBoxEdad.Name = "textBoxEdad";
        	this.textBoxEdad.Size = new System.Drawing.Size(100, 20);
        	this.textBoxEdad.TabIndex = 6;
        	// 
        	// listViewAlumnos
        	// 
        	this.listViewAlumnos.Location = new System.Drawing.Point(351, 37);
        	this.listViewAlumnos.Name = "listViewAlumnos";
        	this.listViewAlumnos.Size = new System.Drawing.Size(378, 104);
        	this.listViewAlumnos.TabIndex = 8;
        	this.listViewAlumnos.UseCompatibleStateImageBehavior = false;
        	// 
        	// buttonMostrar
        	// 
        	this.buttonMostrar.Location = new System.Drawing.Point(397, 198);
        	this.buttonMostrar.Name = "buttonMostrar";
        	this.buttonMostrar.Size = new System.Drawing.Size(119, 23);
        	this.buttonMostrar.TabIndex = 9;
        	this.buttonMostrar.Text = "Mostrar Alumnos";
        	this.buttonMostrar.UseVisualStyleBackColor = true;
        	this.buttonMostrar.Click += new System.EventHandler(this.buttonMostrar_Click);
        	// 
        	// Form1
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(782, 261);
        	this.Controls.Add(this.buttonMostrar);
        	this.Controls.Add(this.listViewAlumnos);
        	this.Controls.Add(this.textBoxEdad);
        	this.Controls.Add(this.textBoxNombre);
        	this.Controls.Add(this.textBoxId);
        	this.Controls.Add(this.labelEdad);
        	this.Controls.Add(this.labelNombre);
        	this.Controls.Add(this.labelId);
        	this.Controls.Add(this.buttonIngresar);
        	this.Name = "Form1";
        	this.Text = "Ingresar Alumno";
        	this.Load += new System.EventHandler(this.Form1_Load);
        	this.ResumeLayout(false);
        	this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonIngresar;
        private System.Windows.Forms.Label labelId;
        private System.Windows.Forms.Label labelNombre;
        private System.Windows.Forms.Label labelEdad;
        private System.Windows.Forms.TextBox textBoxId;
        private System.Windows.Forms.TextBox textBoxNombre;
        private System.Windows.Forms.TextBox textBoxEdad;
        private System.Windows.Forms.ListView listViewAlumnos;
        private System.Windows.Forms.Button buttonMostrar;
    }
}

